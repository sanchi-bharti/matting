# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 14:46:00 2019

@author: Administrator
"""

from os.path import exists,isfile,isdir,join
from os import listdir,walk,makedirs
import cv2
import numpy as np

#img_path=r'C:/Users/Administrator/Desktop/0805546504544_1_garment.jpg'
#trimap_path=r'C:/Users/Administrator/Desktop/0805546504544_1_trimap.jpg'

def find_roi(img,trimap,tolerance=100):

	assert len(trimap.shape)==2 "Expecting shape len 2"
	assert len(img.shape)==3 "Expecting img shape len 3"
    
	#if isfile(img_path) and isfile(trimap_path):
	#img=cv2.imread(img_path)

	img=img.astype(np.float64)

	#trimap=cv2.imread(trimap_path,-1)
	trimap=trimap.astype(np.float64)
	trimap_alpha=trimap[:,:]
	trimap_alpha_gray_loc=np.where(trimap_alpha>100)
	#print(trimap_alpha_gray_loc)
	r_min,r_max=min(trimap_alpha_gray_loc[0]),max(trimap_alpha_gray_loc[0])
	c_min,c_max=min(trimap_alpha_gray_loc[1]),max(trimap_alpha_gray_loc[1])

	#print(r_min,r_max,c_min,c_max)

	r_min_100=0
	r_max_100=trimap.shape[0]
	c_min_100=0
	c_max_100=trimap.shape[1]

	if not (r_min-tolerance)<0:
	    r_min_100=r_min-tolerance
	if not (r_max+tolerance)>trimap.shape[0]:
	    r_max_100=r_max+tolerance
	if not (c_min-tolerance)<0:
	    c_min_100=c_min-tolerance
	if not (c_max+tolerance)>trimap.shape[1]:
	    c_max_100=c_max+tolerance
	    
	trimap_roi=trimap_alpha[r_min_100:r_max_100,c_min_100:c_max_100]
	img_roi=img[r_min_100:r_max_100,c_min_100:c_max_100,:]

	return img_roi,trimap_roi,(trimap_alpha.shape[0],trimap_alpha.shape[1]),(r_min_100,c_min_100,r_max_100,c_max_100)
    
#        cv2.imwrite('C:/Users/Administrator/Desktop/0805546504544_1_trimap_roi.jpg',trimap_roi)
#        cv2.imwrite('C:/Users/Administrator/Desktop/0805546504544_1_garment_roi.jpg',img_roi)
    
    
