# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 15:27:13 2019

@author: Administrator
"""

from matting import alpha_matting, load_image, save_image, estimate_foreground_background, stack_images
from preprocess_alpha_matting import find_roi
from create_trimap import getTrimap1Ch
import cv2
import numpy as np

#img_path='7314780039598.tif'
#mask_path='7314780039598.png'
#mask=cv2.imread(mask_path,0)

#trimap_path='out/7314780039598-trimap.png'
#res_alpha_path='out/7314780039598-alpha.png'
#res_rgb_path='out/7314780039598-rgb.png'

#tri=getTrimap1Ch(mask)

#cv2.imwrite(trimap_path,tri,[cv2.IMWRITE_PNG_COMPRESSION,0])

def matting_alpha(img,trimap,method="ifm", preconditioner="jacobi" , print_info=False):

	#if res_alpha_path.rsplit('.',1)[1].lower()=='png' and res_rgb_path.rsplit('.',1)[1].lower()=='png':

	image_roi,trimap_roi,img_shape,bbox=find_roi(img=img,trimap=trimap)

	alpha_roi = alpha_matting(image_roi, trimap_roi, method=method, preconditioner=preconditioner, print_info=print_info)

	#orig_img=cv2.imread(img_path)/255.0
	orig_img=img
	alpha=np.zeros((img_shape))
	alpha[bbox[0]:bbox[2],bbox[1]:bbox[3]]=alpha_roi

	#result_alpha = np.clip(alpha * 255, 0, 255).astype(np.uint8)
	#cv2.imwrite(res_alpha_path,result_alpha,[cv2.IMWRITE_PNG_COMPRESSION,0])

	orig_img_b_channel,orig_img_g_channel,orig_img_r_channel=cv2.split(orig_img)

	#result=cv2.merge((orig_img_b_channel,orig_img_g_channel,orig_img_r_channel,alpha))
	#result = np.clip(result * 255, 0, 255).astype(np.uint8)

	foreground_roi, background = estimate_foreground_background(image_roi, alpha_roi, print_info=print_info)

	orig_img[bbox[0]:bbox[2],bbox[1]:bbox[3],:]=foreground_roi
	orig_img_b_channel,orig_img_g_channel,orig_img_r_channel=cv2.split(orig_img)

	result_foreground=cv2.merge((orig_img_b_channel,orig_img_g_channel,orig_img_r_channel,alpha))
	#result_foreground = np.clip(result_foreground * 255, 0, 255).astype(np.uint8)

	#cv2.imwrite(res_rgb_path,result_foreground,[cv2.IMWRITE_PNG_COMPRESSION,0])
	#return result_alpha,result_foreground
	return alpha,result_foreground

	#cv2.imwrite("out/"+img_path.replace('\\','/').split('/')[-1].rsplit('.',1)[0]+'-result2.png',result)
	#cv2.imwrite("out/"+img_path.replace('\\','/').split('/')[-1].rsplit('.',1)[0]+'-result_foreground.png',result_foreground)
	#else:
	#print('Output images must be png')
#matting_alpha(img_path,trimap_path,res_alpha_path,res_rgb_path)
